package com.yeshtery.task.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yeshtery.task.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Integer> {
    Optional<UserModel> findByEmailAndPassword(String email, String password);

    Optional<UserModel> findFirstByEmail(String email);
}
