package com.yeshtery.task.service;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.yeshtery.task.model.ProductModel;
import com.yeshtery.task.repository.ProductRepository;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepo;

    public void saveProductToDB(MultipartFile file, String Category, String description, Boolean status) {
        ProductModel product = new ProductModel();
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        String fileExtensions = ".jpg,.png,.gif";
        int lastIndex = fileName.lastIndexOf('.');
        String subString = fileName.substring(lastIndex, fileName.length());
        if (!fileExtensions.contains(subString.toLowerCase())) {
            System.out.println("not a a valid file");
        } else {
            try {
                product.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            product.setName(fileName);
            product.setCategory(Category);
            product.setDescription(description);
            product.setDeleted(status);
            product.setApproved(status);
            productRepo.save(product);
        }
    }

    public List<ProductModel> getAllProduct() {
        return productRepo.findAll();
    }

    public void approveProduct(Long id, Boolean status) {
        ProductModel product = new ProductModel();
        product = productRepo.findById(id).get();
        product.setApproved(status);
        productRepo.save(product);
    }

    public void rejectProduct(Long id, Boolean status) {
        ProductModel product = new ProductModel();
        product = productRepo.findById(id).get();
        product.setDeleted(status);
        productRepo.save(product);
    }

}
