package com.yeshtery.task.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yeshtery.task.model.UserModel;
import com.yeshtery.task.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserModel registerUser(String login, String password, String email) {
        if (login != null && password != null) {
            if(userRepository.findFirstByEmail(email).isPresent()){
                System.out.println("dublicate!");
                return null;
            }
            UserModel userModel = new UserModel();
            userModel.setPassword(password);
            userModel.setEmail(email);
            userModel.setRole("USER");
            return userRepository.save(userModel);
        } else {
            return null;
        }
    }

    public UserModel authentication(String email, String password){
        return userRepository.findByEmailAndPassword(email, password).orElse(null);
    }
}
