package com.yeshtery.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.yeshtery.task.model.ProductModel;
import com.yeshtery.task.service.ProductService;

@Controller
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/addProduct", method = RequestMethod.POST)
    public String saveProduct(@RequestParam("file") MultipartFile file,
            @RequestParam("desc") String desc,
            @RequestParam("user") String user,
            @RequestParam("Category") String Category) {
        productService.saveProductToDB(file, Category, desc, false);
        return "redirect:/getData/" + user;
    }

    @RequestMapping(value = "/approveProduct", method = RequestMethod.POST)
    public String approveProduct(@RequestParam("approved") String data) {
        String[] splitedData = data.split(",");
        productService.approveProduct(Long.parseLong(splitedData[0]), true);
        return "redirect:/getData/" + splitedData[1];
    }

    @RequestMapping(value = "/deleteProduct", method = RequestMethod.POST)
    public String rejectProduct(@RequestParam("rejected") String data) {
        String[] splitedData = data.split(",");
        productService.rejectProduct(Long.parseLong(splitedData[0]), true);
        return "redirect:/getData/" + splitedData[1];
    }

    @RequestMapping(value = "/getData/{user}", method = RequestMethod.GET)
    public String showData(@PathVariable("user") String user, @ModelAttribute ProductModel prodModel, Model model) {
        model.addAttribute("user", user);
        List<ProductModel> products = productService.getAllProduct();
        List<ProductModel> AppProducts = productService.getAllProduct();
        AppProducts.clear();
        if (user.equals("admin")) {
            for (ProductModel product : products) {
                if (product.getApproved() == false && product.getDeleted() == false) {
                    AppProducts.add(product);
                }
            }
            model.addAttribute("products", AppProducts);
            return "adminPage";
        } else if (user.equals("indexPage")) {
            for (ProductModel product : products) {
                if (product.getApproved() == true && product.getDeleted() == false) {
                    AppProducts.add(product);
                }
            }
            model.addAttribute("products", AppProducts);
            return "index";
        }

        else {
            for (ProductModel product : products) {
                if (product.getApproved() == true && product.getDeleted() == false) {
                    AppProducts.add(product);
                }
            }
            model.addAttribute("products", AppProducts);
            return "userPage";
        }
    }

}
