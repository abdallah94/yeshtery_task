package com.yeshtery.task;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class YeshteryTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testAdmin() throws Exception {
        mockMvc.perform(get("http://localhost:8080/getData/admin")).andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("adminPage"));
    }

    @Test
    public void testUser() throws Exception {
        mockMvc.perform(get("http://localhost:8080/getData/${}")).andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("userPage"));
    }

    @Test
    public void testLogin() throws Exception {
        mockMvc.perform(get("http://localhost:8080/login")).andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("login"));
    }

    @Test
    public void testRegister() throws Exception {
        mockMvc.perform(get("http://localhost:8080/register")).andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("register"));
    }
}
